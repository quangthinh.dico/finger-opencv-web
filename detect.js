function rounded_rectangle(src, topLeft, bottomRight) {
  const cornerRadius = Math.round((bottomRight.x - topLeft.x) / 2);
  const p1 = new cv.Point(topLeft.x, topLeft.y + cornerRadius);
  const p3 = bottomRight;

  cv.circle(
    src,
    new cv.Point(p1.x + cornerRadius, p1.y),
    cornerRadius,
    new cv.Scalar(255),
    cv.FILLED,
    cv.LINE_AA
  );
  cv.rectangle(src, p1, p3, new cv.Scalar(255), cv.FILLED, cv.LINE_AA);
}

function detectFingerPrint(tmpImg) {
  // const img = cv.matFromImageData(imageData);

  const POSITIONS = [0.1, 0, 0.9, 1];

  // crop region
  const croppedImg = new cv.Mat();
  const croppedMask = cv.Mat.zeros(tmpImg.size(), cv.CV_8U); // img;

  const maskTopLeft = new cv.Point(
    POSITIONS[0] * croppedMask.cols,
    POSITIONS[1] * croppedMask.rows
  );
  const maskBottomRight = new cv.Point(
    POSITIONS[2] * croppedMask.cols,
    POSITIONS[3] * croppedMask.rows
  );
  rounded_rectangle(croppedMask, maskTopLeft, maskBottomRight);
  tmpImg.copyTo(croppedImg, croppedMask);

  let handImg = new cv.Mat();

  cv.cvtColor(croppedImg, handImg, cv.COLOR_RGBA2RGB);
  cv.cvtColor(handImg, handImg, cv.COLOR_RGB2HSV);

  let lowScalar = new cv.Scalar(0, 48, 80);
  let highScalar = new cv.Scalar(14, 255, 255);
  let low = new cv.Mat(handImg.rows, handImg.cols, handImg.type(), lowScalar);
  let high = new cv.Mat(handImg.rows, handImg.cols, handImg.type(), highScalar);
  cv.inRange(handImg, low, high, handImg);

  const erosion_size = 0;
  const MAT_ELEMENT = cv.getStructuringElement(
    cv.MORPH_RECT,
    new cv.Size(2 * erosion_size + 1, 2 * erosion_size + 1),
    new cv.Point(erosion_size, erosion_size)
  );

  cv.erode(handImg, handImg, MAT_ELEMENT);
  cv.dilate(handImg, handImg, MAT_ELEMENT);

  let contours = new cv.MatVector();
  let hierarchy = new cv.Mat();
  cv.findContours(handImg, contours, hierarchy, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE, {
    x: 0,
    y: 0,
  });

  if (contours.size() === 0) {
    handImg = croppedImg.clone();
  } else {
    let largestContour = 0;

    for (let i = largestContour + 1; i < contours.size(); ++i) {
      if (cv.contourArea(contours.get(i)) > cv.contourArea(contours.get(largestContour))) {
        largestContour = i;
      }
    }

    let mask = cv.Mat.zeros(croppedImg.size(), cv.CV_8U);
    cv.drawContours(
      mask,
      contours,
      largestContour,
      new cv.Scalar(1),
      cv.FILLED,
      cv.LINE_AA,
      hierarchy
    );

    croppedImg.copyTo(handImg, mask);
    let bb = cv.boundingRect(contours.get(largestContour));
    handImg = handImg.roi(bb);
  }

  // cv.detailEnhance(handImg, handImg); // ? why not wokr /

  const gray_src = new cv.Mat();
  const overlay_src = new cv.Mat();
  cv.cvtColor(handImg, gray_src, cv.COLOR_BGR2GRAY);

  cv.GaussianBlur(gray_src, overlay_src, new cv.Size(0, 0), 3);
  cv.addWeighted(gray_src, 2.5, overlay_src, -1.5, 0, overlay_src);

  cv.adaptiveThreshold(
    overlay_src,
    handImg,
    255,
    cv.ADAPTIVE_THRESH_GAUSSIAN_C,
    cv.THRESH_BINARY,
    13,
    1
  );

  gray_src.delete();
  overlay_src.delete();
  contours.delete();
  hierarchy.delete();

  return handImg;
}
